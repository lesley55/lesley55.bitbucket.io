var randomWordArr = ["gymzaal", "jazzzanger", "javascript", "picknick", "hyena", "quiz", "zuivel", "kitsch", "sfynx", "galgje", "dodo", "hippopotomonstrosesquippedaliofobie", "hottetottesoldatententendoekententoonstellingstereinmuseumtegelplaatjesschilder", "krokodillentranen", "belastingrechtadviseurkantoor", "lucifer", "mediaafhankelijkheidssysteem", "qua", "productieaansprakelijkheidsverzekeringen"];
var randomWord = randomWordArr[Math.floor(Math.random() * randomWordArr.length)];

var s;
var count = 0;
var answerArray = [];

function startUp() {
  for (var i = 0; i < randomWord.length; i++) {
    answerArray[i] = "_";
  }
  s = answerArray.join(" ");
  document.getElementById("answer").innerHTML = s;
}

function Letter() {
  var letter = document.getElementById("letter").value;
  if (letter.length > 0) {
    for (var i = 0; i <randomWord.length; i++) {
      if (randomWord[i] === letter) {
        answerArray[i] = letter;
      }
    }
    count++;
    document.getElementById("counter").innerHTML = "Aantal keer geraden: " + count;
    document.getElementById("answer").innerHTML = answerArray.join(" ");
  }
  if(count >= randomWord.length) {
    document.getElementById("irritant").innerHTML = "Je had het al lang kunnen raden, dit valt me een beetje tegen.";
  }
}
